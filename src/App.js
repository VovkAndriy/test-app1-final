import React from "react";
import axios from 'axios';

import './App.css';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      values: Array(30).fill(''),
      emptyvalues: Array(30).fill(''),
      articleId: null,
    };
    this.afterSubmission = this.afterSubmission.bind(this);
    this.resultHandler = this.resultHandler.bind(this);
  }

  createUI() {
    return this.state.values.map((el, i) =>
      <div key={i} className="style">
        <input type="text" value={el || ''} onChange={this.resultHandler.bind(this, i)} />
        <input type="button" value={'X'} onClick={() => this.handleClearInput(i)} />
      </div>
    )
  }


  handleClearInput(i) {
    let values = [...this.state.values];
    values[i] = '';
    this.setState({ values });
    // console.log(this.state.values, 'handleClearInput')
  }
  handleClearAllInput() {
    this.setState({ values: this.state.emptyvalues });
    // console.log(this.state.values, 'handleClearInput')
  }
  resultHandler(i, e) {
    let values = [...this.state.values];
    values[i] = e.target.value;
    this.setState({ values })
    // console.log(this.state.values)
  }

  afterSubmission(event) {
    let ArrToObjg = Object.assign({}, this.state.values);
    console.log('A name was submitted: ' + ArrToObjg);
    const article = ArrToObjg;
    axios.post('http://localhost:3004/data', article)
      .then(response => this.setState({ articleId: response.data.id }))
      .catch(error => {
        this.setState({ errorMessage: error.message });
        console.error('There was an error!', error);
      });
    event.preventDefault();
  }

  render() {
    return <div className="App" >
      <form className="form-main" onSubmit={this.afterSubmission}>
        {this.createUI()}
        <div className="buttons">
          <button type="submit" >submit</button>
          <input type="button" className="clear" value={'Clear'} onClick={() => this.handleClearAllInput()} />
        </div>
      </form >
    </div >
  }
}

export default App;
